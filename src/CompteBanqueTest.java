import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CompteBanqueTest {

	CompteBanque compte;
	int solde;

	@Before

	public void setUp() {

		compte = new CompteBanque();
		solde = 0;

	}

	@Test(expected = Exception.class)
	public void testCompteBanque() throws Exception {

		solde = -100;

		CompteBanque compte = new CompteBanque(solde);

		// assertTrue("possitive solde", compte.getSolde() >= 0);

	}

	// meme chose pour toutes les methodes avec parametre
	@Test(expected = Exception.class)
	public void testAddCreditParameter() throws Exception {

		// positif parameter
		compte.addCredit(-10);

	}

	@Test
	public void testAddCredit() throws Exception {

		int ancienSolde = compte.getSolde();

		assertTrue("reel credit", compte.addCredit(10) == ancienSolde + 10);

	}

	@Test(expected = Exception.class)
	public void testSubDebitInfSolde() throws Exception {

		int debit = 30;

		compte.setSolde(20);

		assertTrue("new credit", compte.subDebit(debit) >= 0);

	}

	/*
	 * @Test public void testVire() { fail("Not yet implemented"); }
	 */

}
