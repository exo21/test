
public class CompteBanque {

	private int solde;

	public CompteBanque() {

		this.solde = 0;

	}

	public CompteBanque(int solde) throws Exception {

		if (solde < 0)
			throw new Exception("Le solde ne peut pas �tre negatif");
		else
			this.solde = solde;

	}

	public int getSolde() {
		return this.solde;
	}

	public void setSolde(int solde) throws Exception {

		if (solde < 0)
			throw new Exception("Le solde ne peut pas �tre negatif");
		else
			this.solde = solde;
	}

	public int addCredit(int credit) throws Exception {

		if (credit < 0)
			throw new Exception("Le credit ne peut pas �tre negatif");
		else
			this.solde += credit;

		return this.solde;
	}

	public int subDebit(int debit) throws Exception {

		if (debit < 0)
			throw new Exception("Le debit ne peut pas �tre negatif");

		if (debit > this.solde)
			throw new Exception("Le debit ne peut pas �tre superieur au solde");

		this.solde -= debit;
		return this.solde;
	}

	public int vire(CompteBanque compteDestination, int somme) throws Exception {

		if (somme < 0)
			throw new Exception("La somme ne peut pas �tre negative");
		else

			this.solde -= somme;

		compteDestination.solde += somme;

		return this.solde;

	}

}
